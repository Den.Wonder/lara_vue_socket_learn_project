@extends('layouts.app')

@section('content')

    <div class="container">
            <div class="row">
                Q!
                <chartline-component></chartline-component>
            </div>
            <div class="row">
                Q#2!
                <chartpie-component></chartpie-component>
            </div>
        <div class="row">
            Q#3!
            <chartrandom-component></chartrandom-component>
        </div>
        <div class="row">
            <socket-chart-component></socket-chart-component>
        </div>
    </div>

@endsection

