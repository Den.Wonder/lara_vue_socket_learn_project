@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    hello
                    <a href="graphs"><button class="btn btn-danger">GRAPHS!</button> </a>
                </div>
                <div id="app">
                    <div class="container">
                        <example-component></example-component>
                        <div class="row d-flex">

                            <div class="col-12">
                                <prop-component v-bind:urldata="{{json_encode($url_data)}}"></prop-component>
                            </div>

                        </div>
                    </div>
                    <div class="container border">
                        <div class="text-center">
                            <div class="row">
                                <ajax-component></ajax-component>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
