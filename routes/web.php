<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/get-json','HomeController@getJson')->name('getJson');
Route::get('/graphs', 'GraphsController@index')->name('graphs');
Route::get('/graphs/data-chart', 'GraphsController@dataChart')->name('dataChart');
Route::get('/graphs/data-rand', 'GraphsController@dataRand')->name('dataRand');
Route::get('/graphs/socket-chart', 'GraphsController@newEvent')->name('socketChart');

