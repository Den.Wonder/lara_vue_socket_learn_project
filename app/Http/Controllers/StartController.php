<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StartController extends Controller
{
    public function index(){
        $url_data = [
            array(
                'title'=>'first_obj_title',
                'url'=>'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
            ),
            array(
                'title'=>'second_obj',
                'url'=>'https://www.youtube.com/watch?v=rY0WxgSXdEE'
            )
        ];
        return view('start',[
           'url_data' => $url_data
        ]);
    }
}

