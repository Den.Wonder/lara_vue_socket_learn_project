<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Events\newEvent;


class GraphsController extends Controller
{
    public function index(){
        return view('graphs');
    }

    public function dataChart(){
        return[
            'labels'=>['March', 'April', 'May', 'June', 'Jule'],
            'datasets'=>array([
                'label'=>'Sends',
                'backgroundColor'=>['#f26202','#36f014', '#87f6a5','#990011','#142648'],
                'data'=>[10000,15000,5000,12000,1000],
            ],
                [
                    'label'=>'Buyer',
                    'backgroundColor'=>['#f26202','#36f014', '#87f6a5','#990011','#142648'],
                    'data'=>[12000,11000,8000,2000,5000],
                ]
                )
        ];
    }

    public function dataRand(){
        return[
            'labels'=>['March', 'April', 'May', 'June', 'Jule'],
            'datasets'=>array(
                [
                    'label'=>'gold',
                    'backgroundColor'=>'#16AB39',
                    'data'=>[rand(0,40000),rand(0,40000),rand(0,40000),rand(0,40000),rand(0,40000)]
                ],
                [
                    'label'=>'silver',
                    'backgroundColor'=>'#B5CC18',
                    'data'=>[rand(0,40000),rand(0,40000),rand(0,40000),rand(0,40000),rand(0,40000)]
                ]
            )
        ];
    }

    public function newEvent(Request $request){
        $result = [
            'labels'=>['March', 'April', 'May', 'June', 'Jule'],
            'datasets'=>array([
                'label'=>'Sends',
                'backgroundColor'=>['#f26202'],
                'data'=>[10000,15000,5000,12000,1000],
            ])
            ];
        if($request->has('label')){
            $result['labels'][] = $request->input('label');
            $result['datasets'][0]['data'][]=(integer)$request->input('sale');

            if($request->has('realtime')){
                if(filter_var($request->input('realtime'), FILTER_VALIDATE_BOOLEAN)){
                    event(new newEvent($result));
                }
            }
        }
    return $result;
    }


}
