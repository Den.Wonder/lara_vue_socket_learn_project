<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $url_data = [
            array(
                'title'=>'first_obj_title',
                'url'=>'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
            ),
            array(
                'title'=>'second_obj',
                'url'=>'https://www.youtube.com/watch?v=rY0WxgSXdEE'
            )
        ];
        return view('home',[
            'url_data' => $url_data
        ]);
    }

    public function getJson(){
        return [
            array(
                'title' => 'Google',
                'url' => 'https://google.com'
            ),
          array(
              'title' => 'Yandex',
              'url' => 'https://yandex.ru'
          )
        ];
    }
}
